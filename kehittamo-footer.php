<?php
/*
Plugin Name: Kehittämö Footer
Plugin URI: http://www.kehittamo.fi
Description: Add Kehittämö footer to your site
Version: 0.1
Author: Kehittämö Oy / Janne Saarela
Author Email: asiakaspalvelu@kehittamo.fi
License: GPL2

  Copyright 2016 Kehittämö Oy (asiakaspalvelu@kehittamo.fi)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/


namespace Kehittamo\Plugins\Footer;

// Define all our constants
define('Kehittamo\Plugins\Footer\PLUGIN_PATH', plugin_dir_path(__FILE__));
define('Kehittamo\Plugins\Footer\PLUGIN_URL', plugin_dir_url(__FILE__));
define('Kehittamo\Plugins\Footer\FOOTER_SLUG', 'kehittamo-footer');

class Load
{

	/**
	 * Construct the plugin
	 */
	function __construct()
	{

		add_action('plugins_loaded', array($this, 'load_plugin'));

		add_action('wp_enqueue_scripts', array($this, 'wp_enqueue_scripts'));

		register_activation_hook(__FILE__, array($this, 'init_plugin'));

		register_deactivation_hook(__FILE__, array($this, 'deactivate_plugin'));

	}

	/**
	 * Init
	 */
	function init_plugin() {
	}

	function deactivate_plugin() {
	}

	/**
	 * Load the plugin and its dependencies
	 */
	function load_plugin() {
		$this->load_textdomain();
		$this->admin();
		add_action('init', array($this, 'front_end'));
	}

	/**
	 * Load plugin textdomain.
	 */
	function load_textdomain() {
		load_plugin_textdomain(FOOTER_SLUG, false, dirname(plugin_basename(__FILE__)) . '/languages');
	}

	/**
	 * Load front end
	 */
	function front_end() {

		// Load front end
		require_once(PLUGIN_PATH . 'includes/kehittamo-footer-frontend.php');

	}

	/**
	 * Plugin admin page
	 */
	function admin() {

		// Load plugin options page
		require_once(PLUGIN_PATH . '/includes/kehittamo-footer-admin.php');

	}


	/**
	 * Load Frontend Styles
	 */
	function wp_enqueue_scripts() {

			// Javascript
			// wp_register_script( 'kehittamo-footer', PLUGIN_URL .'includes/js/kehittamo-footer.min.js', array( 'jquery' ), null, true );
			// wp_enqueue_script( 'kehittamo-footer' );

			//CSS Styles
			wp_register_style('kehittamo-footer-frontend', PLUGIN_URL . 'includes/css/kehittamo-footer-frontend.min.css');
			wp_enqueue_style('kehittamo-footer-frontend');
	}
}

$kehittamo_ads = new \Kehittamo\Plugins\Footer\Load();
