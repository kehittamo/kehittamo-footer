<?php
namespace Kehittamo\Plugins\Footer;

class SettingsPage
{
	/**
	 * Holds the values to be used in the fields callbacks
	 */
	private $options;

	/**
	 * Start up
	 */
	public function __construct() {
		add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
		add_action( 'admin_init', array( $this, 'page_init' ) );
	}

	/**
	 * Add options page
	 */
	public function add_plugin_page() {
		// This page will be under "Settings"
		add_options_page(
			'Kehittämö Footer Settings',
			'Kehittämö Footer',
			'manage_options',
			'kehittamo-footer-settings',
			array( $this, 'create_admin_page' )
		);
	}

	/**
	 * Options page callback
	 */
	public function create_admin_page() {
		// Set class property.
		$this->options = get_option( 'kehittamo_footer_settings_name' );
		?>
		<div class="wrap">
			<h1>Kehittämö Footer</h1>
			<form method="post" action="options.php">
				<?php
				// This prints out all hidden setting fields.
				settings_fields( 'kehittamo_footer_settings_group' );
				do_settings_sections( 'kehittamo-footer-settings' );
				submit_button();
				?>
			</form>
		</div>
		<?php
	}

	/**
	 * Register and add settings
	 */
	public function page_init() {
		register_setting(
			'kehittamo_footer_settings_group', // Option group
			'kehittamo_footer_settings_name', // Option name
			array( $this, 'sanitize' ) // Sanitize
		);

		add_settings_section(
			'kehittamo_footer_settings_default', // ID
			__( 'Footer Fields', 'kehittamo-footer'), // Title
			array( $this, 'print_section_info' ), // Callback
			'kehittamo-footer-settings' // Page
		);

		add_settings_field(
			'organization', // ID
			__( 'Content (name of organization)', 'kehittamo-footer' ), // Title
			array( $this, 'settings_field_callback' ), // Callback
			'kehittamo-footer-settings', // Page
			'kehittamo_footer_settings_default', // Section
			array ('id' => 'organization')
		);

		add_settings_field(
			'editor_name',
			__( 'Editor (name)', 'kehittamo-footer'),
			array( $this, 'settings_field_callback' ),
			'kehittamo-footer-settings',
			'kehittamo_footer_settings_default',
			array ('id' => 'editor_name')
		);
		add_settings_field(
			'business',
			__( 'Business ID', 'kehittamo-footer'),
			array( $this, 'settings_field_callback' ),
			'kehittamo-footer-settings',
			'kehittamo_footer_settings_default',
			array ('id' => 'business')
		);
		add_settings_field(
			'contact_url',
			__( 'Contact Page (url)', 'kehittamo-footer'),
			array( $this, 'settings_field_callback' ),
			'kehittamo-footer-settings',
			'kehittamo_footer_settings_default',
			array ('id' => 'contact_url')
		);
	}

	/**
	 * Sanitize each setting field as needed
	 *
	 * @param array $input Contains all settings fields as array keys
	 */
	public function sanitize( $input ) {
		$new_input = array();

		// Sanitize each input field.
		foreach ( $input as $key => $value ) {
			$new_input[ $key ] = sanitize_text_field( $value);
		}
		// Sanitize url.
		if ( isset( $input['contact_url'] ) ) {
			$new_input['contact_url'] = esc_url( $input['contact_url'] );
		}

		return $new_input;
	}

	/**
	 * Print the Section text
	 */
	public function print_section_info() {
		print __( 'Enter your settings below:', 'kehittamo-footer');
	}

	/**
	 * General settings field callback function to print input field and set value
	 */
	public function settings_field_callback( $args ) {
		$id = $args['id'];
		printf(
			'<input type="text" id="'. $id . '" name="kehittamo_footer_settings_name[' . $id . ']" value="%s" />',
			isset( $this->options[''. $id . ''] ) ? esc_attr($this->options[''. $id . '']) : ''
		);
	}

}

if ( is_admin() ) {
	$kehittamo_footer_settings_page = new SettingsPage();
}