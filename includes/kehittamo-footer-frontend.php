<?php

namespace Kehittamo\Plugins\Footer;


class FrontEnd{

    /**
     * Holds the options of the plugin
     */
    private $options;

    /**
     * Start up
     */
    public function __construct(){
        // Set options
        $this->options = get_option( 'kehittamo_footer_settings_name' );
        // Add wp_footer hook and fire as late as possible
        add_action( 'wp_footer', array( $this, 'add_footer' ), 9999 );
    }

    /**
    * Add footer
    *
    * @return html
    */
    public function add_footer(){ ?>
      <footer class='kehittamo-footer'>
        <section class='kehittamo-footer__custom-info'>
          <ul>
			<?php if ( !empty( $this->options['organization'] ) ) : ?>
			  <li><?php echo __( 'content', 'kehittamo-footer' ) . ': ' . $this->options['organization']; ?></li>
			<?php endif; ?>
			<?php if ( !empty( $this->options['editor_name'] ) ) : ?>
			  <li><?php echo __( 'editor', 'kehittamo-footer' ) . ': ' . $this->options['editor_name']; ?></li>
			<?php endif; ?>
			<?php if ( !empty( $this->options['business'] ) ) : ?>
			  <li><?php echo __( 'business', 'kehittamo-footer' ) . ': ' .  $this->options['business']; ?></li>
			<?php endif; ?>
			<?php if ( !empty( $this->options['contact_url'] ) ) : ?>
			  <li><a href="<?php echo $this->options['contact_url']; ?>"><?php echo __( 'contact', 'kehittamo-footer' ) ?></a></li>
			<?php endif; ?>
          </ul>
        </section>
        <div class='kehittamo-footer__wrapper'>
          <section class='kehittamo-footer__logo'>
            <a target="_blank" title="Kehitt�m�.fi" href="https://kehittamo.fi"><?php require_once( PLUGIN_PATH . 'includes/svg/kehittamo-logo.php' ); ?></a>
          </section>
          <section class='kehittamo-footer__links'>
            <a target="_blank"href="https://kehittamo.fi/yllapito-ja-tukipalvelu/"><?php _e( 'Site maintenance and help desk service', FOOTER_SLUG ); ?></a>
            <a target="_blank"href="https://kehittamo.fi/rekisteri-ja-tietosuojaseloste/"><?php _e( 'Register statement and privacy policy ', FOOTER_SLUG ); ?></a>
            <a target="_blank"href="https://kehittamo.fi/evasteet"><?php _e( 'Cookies', FOOTER_SLUG ); ?></a>
          </section>
          <section class='kehittamo-footer__some'>
            <a target="_blank"title="Facebook" href="https://facebook.com/kehittamo"><?php require_once( PLUGIN_PATH . 'includes/svg/facebook-logo.php' ); ?></a>
            <a target="_blank"title="Twitter" href="https://twitter.com/kehittamo"><?php require_once( PLUGIN_PATH . 'includes/svg/twitter-logo.php' ); ?></a>
            <a target="_blank"title="LinkedIn" href="https://www.linkedin.com/company/digitoimisto-kehitt%C3%A4m%C3%B6"><?php require_once( PLUGIN_PATH . 'includes/svg/linkedin-logo.php' ); ?></a>
          </section>
          <section class='kehittamo-footer__login'>
            <?php if( is_user_logged_in() ){
              $url = wp_logout_url();
              $loginout_text = __( 'Log out', FOOTER_SLUG );
            } else{
              $url = wp_login_url();
              $loginout_text = __( 'Login', FOOTER_SLUG );
            }?>
            <a href="<?php echo $url; ?>"><?php echo $loginout_text; ?></a>
          </section>
        </div>
      </footer>
    <?php
    }

}

$kehittamo_footers_front_end = new \Kehittamo\Plugins\Footer\FrontEnd();
